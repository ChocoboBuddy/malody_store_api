using System;
using System.Security.Cryptography;

namespace Example {
   class KeyCheck {
      static void Main(string[] args) {
         var uid = "";  //get from url query string
         var key = "";  //get from url query string
         var publickey = File.ReadAllText(@"rsa_public.bin");
         var rsa = new RSACryptoServiceProvider();
         rsa.FromXmlString(publickey);

         var verified = rsa.VerifyData(Encoding.ASCII.GetBytes(uid), "SHA256", Base64UrlSafeDecode(key));
         if(verified){
             Console.WriteLine("The key is valid");
         }else{
             Console.WriteLine("The key is invalid");
         }
      }

      static byte[] Base64UrlSafeDecode(string raw) {
         raw = raw.Replace('-', '+').Replace('_', '/');
         return Convert.FromBase64String(raw);
      }
   }
}
